import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
export enum OrderStatus {
  CREATED = 1,
  CONFIRMED = 2,
  DELIVERED = 3,
  CANCELLED = 4,
}
@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userId: number;

  @Column()
  productId: number;

  @Column()
  totalAmount: number;

  @Column()
  status: OrderStatus = OrderStatus.CREATED;
}
