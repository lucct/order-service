import { Test, TestingModule } from '@nestjs/testing';
import { OrderController } from './order.controller';
import { Order, OrderStatus } from './order.entity';
import { OrderService } from './order.service';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Connection, ConnectionOptions, Repository } from 'typeorm';
import { User, UserStatus } from '../user/user.entity';
import { Product } from '../product/product.entity';
import { Auth } from '../user/auth.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { UserService } from '../user/user.service';
import { ProductService } from '../product/product.service';
import { createConnection } from 'typeorm';
import * as request from 'supertest';
import * as faker from 'faker';
import * as dotenv from 'dotenv';
dotenv.config();
const connectionConfig: ConnectionOptions = {
  type: 'sqlite',
  database: 'db/database.test.db',
  entities: [__dirname + '/../**/*.entity{.ts,.js}'],
  logging: false,
  synchronize: true,
};
describe('OrderController', () => {
  let app: INestApplication;
  let orderController: OrderController;
  let orderService: OrderService;

  let connection: Connection;
  const DEFAULT_PIN = '1234';
  const mockResponse: any = {
    json: (res) => res,
    status: () => mockResponse,
  };
  beforeAll(async () => {
    connection = await createConnection(connectionConfig);
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ClientsModule.register([
          {
            name: 'ORDER_SERVICE',
            transport: Transport.REDIS,
            options: { url: process.env.REDIS_URL },
          },
          {
            name: 'PAYMENT_SERVICE',
            transport: Transport.REDIS,
            options: { url: process.env.REDIS_URL },
          },
        ]),
      ],
      controllers: [OrderController],
      providers: [
        OrderService,
        UserService,
        ProductService,
        {
          provide: getRepositoryToken(Order),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Product),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Auth),
          useClass: Repository,
        },
      ],
    }).compile();

    orderService = module.get<OrderService>(OrderService);
    orderController = module.get<OrderController>(OrderController);

    app = module.createNestApplication();
  });

  describe('findAll', () => {
    it('should return an array of orders', async (done) => {
      const result = [
        {
          id: 1,
        },
      ];
      jest
        .spyOn(orderService, 'findAll')
        .mockImplementation((): Promise<any> => Promise.resolve(result));

      expect(await orderController.findAll(mockResponse)).toBe(result);
      done();
    });
  });

  describe('Create new Order', () => {
    let userId = null;
    let productId = null;

    it('should successfully create new user in db', async (done) => {
      const authPayload = new Auth();
      Object.assign(authPayload, {
        pin: DEFAULT_PIN,
      });
      const authRepository = connection.getRepository(Auth);
      const auth = await authRepository.save(authPayload);

      expect(typeof auth.id).toBe('number');

      const userPayload = new User();
      Object.assign(userPayload, {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        mobile: faker.phone.phoneNumber(),
        password: faker.internet.password(),
        address: faker.address.streetAddress(),
        status: UserStatus.ACTIVATED,
        credits: 10,
        auth: auth,
      });
      const userRepository = connection.getRepository(User);
      const user = await userRepository.save(userPayload);
      userId = user.id;
      expect(typeof user.id).toBe('number');
      done();
    });
    it('should successfully create new product in db', async (done) => {
      const productPayload = new Product();
      Object.assign(productPayload, {
        name: faker.commerce.productName(),
        price: 5,
        description: faker.commerce.productName(),
        imageUrl: faker.phone.phoneNumber(),
        quantity: 5,
      });
      const productRepository = connection.getRepository(Product);
      const product = await productRepository.save(productPayload);
      productId = product.id;
      expect(typeof product.id).toBe('number');
      done();
    });
    it('should successfully create new order in db', async (done) => {
      expect(typeof userId).toBe('number');
      expect(typeof productId).toBe('number');

      const orderPayload = new Order();
      Object.assign(orderPayload, {
        totalAmount: 2,
        userId: userId,
        productId: productId,
        status: OrderStatus.CREATED,
      });

      const orderRepository = connection.getRepository(Order);
      const order = await orderRepository.save(orderPayload);
      const retrievedOrder = await orderRepository.findOne(order.id);

      expect(retrievedOrder.totalAmount).toBe(2);
      expect(retrievedOrder.userId).toBe(userId);
      expect(retrievedOrder.productId).toBe(productId);
      expect(retrievedOrder.status).toBe(OrderStatus.CREATED);
      done();
    });

    it('should failed create new order in db, when userId/productId/pin is not passed', async (done) => {
      request(app.getHttpServer())
        .post('/orders')
        .send({ productId: undefined, userId: userId, pin: DEFAULT_PIN })
        .expect(HttpStatus.PRECONDITION_FAILED);

      request(app.getHttpServer())
        .post('/orders')
        .send({ productId: productId, userId: undefined, pin: DEFAULT_PIN })
        .expect(HttpStatus.PRECONDITION_FAILED);

      request(app.getHttpServer())
        .post('/orders')
        .send({ productId, userId, pin: undefined })
        .expect(HttpStatus.PRECONDITION_FAILED);
      done();
    });

    it('should failed create new order in db, when userId/productId/pin is not passed', async (done) => {
      request(app.getHttpServer())
        .post('/orders')
        .send({ productId: undefined, userId: userId, pin: DEFAULT_PIN })
        .expect(HttpStatus.PRECONDITION_FAILED);
      done();
    });

    afterAll(async () => {
      const entities = [
        { name: 'Order', tableName: 'order' },
        { name: 'Product', tableName: 'product' },
        { name: 'User', tableName: 'user' },
        { name: 'Auth', tableName: 'auth' },
      ];
      try {
        for (const entity of entities) {
          const repository = await connection.getRepository(entity.name);
          console.log(
            `DELETE FROM \`${entity.tableName}\`;UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='${entity.tableName}';`,
          );
          await repository.query(
            `DELETE FROM \`${entity.tableName}\`;UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='${entity.tableName}';`,
          );
        }
      } catch (error) {
        console.log(error);
        // throw new Error(`ERROR: Cleaning test db: ${error}`);
      }
      await connection.close();
      await app.close();
    });
  });
});
