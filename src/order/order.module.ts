import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './order.entity';
import { Product } from '../product/product.entity';
import { User } from '../user/user.entity';
import { Auth } from '../user/auth.entity';
import { UserService } from '../user/user.service';
import { ProductService } from '../product/product.service';
import { Transport, ClientsModule } from '@nestjs/microservices';
import * as dotenv from 'dotenv';
dotenv.config();

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'ORDER_SERVICE',
        transport: Transport.REDIS,
        options: { url: process.env.REDIS_URL },
      },
      {
        name: 'PAYMENT_SERVICE',
        transport: Transport.REDIS,
        options: { url: process.env.REDIS_URL },
      },
    ]),
    TypeOrmModule.forFeature([Order, User, Product, Auth]),
  ],
  providers: [OrderService, UserService, ProductService],
  controllers: [OrderController],
})
export class OrderModule {}
