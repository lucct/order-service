import {
  Controller,
  Get,
  Post,
  Put,
  Body,
  Param,
  Res,
  Inject,
} from '@nestjs/common';
import { HttpStatus } from '@nestjs/common';
import { OrderService } from './order.service';
import { UserService } from '../user/user.service';
import { Order, OrderStatus } from './order.entity';
import { Response } from 'express';
import { ProductService } from '../product/product.service';
import { ClientProxy } from '@nestjs/microservices';
import { EventPattern } from '@nestjs/microservices';

const ORDER_UPDATE_INTERVAL_IN_MS = 5 * 1000;
@Controller('orders')
export class OrderController {
  constructor(
    @Inject('ORDER_SERVICE') private readonly orderClient: ClientProxy,
    @Inject('PAYMENT_SERVICE') private readonly client: ClientProxy,
    private readonly orderService: OrderService,
    private readonly userService: UserService,
    private readonly productService: ProductService,
  ) {}

  async onApplicationBootstrap() {
    await this.client.connect();
  }

  @Post()
  async create(@Body() req: any, @Res() res: Response) {
    const { productId, userId, authPin } = req;
    // Check if valid request
    if (!productId || !userId || !authPin) {
      return res
        .status(HttpStatus.PRECONDITION_FAILED)
        .json({ status: false, message: 'USER NOT FOUND' });
    }
    const user = await this.userService.findOne(userId);
    if (!user) {
      return res
        .status(HttpStatus.PRECONDITION_FAILED)
        .json({ status: false, message: 'USER NOT FOUND' });
    }

    // Checking if product is present in database
    const product = await this.productService.findOne(productId);
    if (!product) {
      return res
        .status(HttpStatus.PRECONDITION_FAILED)
        .json({ status: false, message: 'PRODUCT NOT FOUND' });
    }

    // Checking if product is in stock
    if (product.quantity === 0) {
      return res
        .status(HttpStatus.OK)
        .json({ status: false, message: 'PRODUCT OUT OF STOCK' });
    }
    const orderPayload = new Order();
    Object.assign(orderPayload, {
      totalAmount: product.price,
      userId: user.id,
      productId: product.id,
      status: OrderStatus.CREATED,
    });
    const order = await this.orderService.create(orderPayload);
    if (order) {
      const response = await this.client
        .send<string>(
          { cmd: 'process_payment' },
          { user, order, product, authPin },
        )
        .toPromise();
      console.log('RESPONSE' + response);
      if (response === 'CONFIRMED') {
        await this.orderService.updateStatus(order.id, OrderStatus.CONFIRMED);

        setTimeout(
          () => {
            this.orderClient
              .emit<string>('deliver_order', { id: order.id })
              .toPromise();
          },
          ORDER_UPDATE_INTERVAL_IN_MS,
          order,
        );

        return res
          .status(HttpStatus.CREATED)
          .json({ status: true, message: 'SUCCESSFUL PAYMENT' });
      } else {
        await this.orderService.updateStatus(order.id, OrderStatus.CANCELLED);
        return res
          .status(HttpStatus.EXPECTATION_FAILED)
          .json({ status: false, message: 'UNSUCCESSFUL PAYMENT' });
      }
    }

    return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ status: false });
  }
  @EventPattern('deliver_order')
  async processPayment(id) {
    const order = await this.orderService.findOne(id);
    if (!!order && order.status === OrderStatus.CONFIRMED) {
      this.orderService.updateStatus(id, OrderStatus.DELIVERED);
    }
  }
  @Put(':id/cancel')
  async cancel(@Param() params, @Res() res: Response) {
    const order = await this.orderService.findOne(params.id);
    if (!!order && order.status !== OrderStatus.CANCELLED) {
      this.orderService.updateStatus(params.id, OrderStatus.CANCELLED);
    }
    res.status(HttpStatus.OK);
  }
  @Get()
  async findAll(@Res() res: Response) {
    const orders = await this.orderService.findAll();
    return res.status(HttpStatus.OK).json(orders);
  }

  @Get(':id')
  async getOne(@Param() params, @Res() res: Response) {
    const order = await this.orderService.findOne(params.id);
    if (!order) {
      return res
        .status(HttpStatus.NOT_FOUND)
        .json({ status: false, message: 'ORDER NOT FOUND' });
    }
    return res.status(HttpStatus.OK).json(order);
  }
}
