import { Injectable } from '@nestjs/common';
import { Order, OrderStatus } from './order.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private readonly orderRepo: Repository<Order>,
  ) {}

  async create(order: Order): Promise<Order> {
    return await this.orderRepo.save(order);
  }
  async findAll(): Promise<Order[]> {
    return await this.orderRepo.find();
  }

  async findOne(id: number): Promise<Order> {
    return await this.orderRepo.findOne(id);
  }

  async updateStatus(id: number, status: OrderStatus) {
    const order = await this.orderRepo.findOne(id);
    order.status = status;
    return await this.orderRepo.save(order);
  }
}
