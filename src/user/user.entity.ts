import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Auth } from './auth.entity';
export enum UserStatus {
  ACTIVATED,
  DEACTIVATED,
}
@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500 })
  firstName: string;

  @Column({ length: 500 })
  lastName: string;

  @Column({ length: 500 })
  email: string;

  @Column({ length: 500 })
  mobile: string;

  @Column({ length: 500 })
  password: string;

  @Column('text')
  address: string;

  @Column({ default: UserStatus.ACTIVATED })
  status: UserStatus;

  // Should be recheck
  @Column()
  credits: number;

  @OneToOne(() => Auth, { eager: true })
  @JoinColumn()
  auth: Auth;
}
