import { ConnectionOptions } from 'typeorm';

import * as path from 'path';
import * as dotenv from 'dotenv';
dotenv.config();

const baseDir = path.join(__dirname, './');
const database = `${process.env.TYPEORM_DATABASE}`;
const entitiesPath = `${baseDir}${process.env.TYPEORM_ENTITIES}`;
const type: any = process.env.TYPEORM_CONNECTION;

// Should recheck
const config: ConnectionOptions = {
  type,
  database,
  synchronize: process.env.TYPEORM_SYNCHRONIZE == 'true' ? true : false,
  logging: process.env.TYPEORM_LOGGING == 'true' ? true : false,
  entities: [entitiesPath],
};
export default config;
