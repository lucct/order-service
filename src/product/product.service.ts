import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './product.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepo: Repository<Product>,
  ) {}

  async findOne(id: number) {
    return await this.productRepo.findOne(id);
  }
  async create(product: Product): Promise<Product> {
    return await this.productRepo.save(product);
  }
}
