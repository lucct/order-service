import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500 })
  name: string;

  @Column()
  price: number;

  @Column('text')
  description: string;

  @Column('text')
  imageUrl: string;

  @Column()
  quantity: number;
}
